# KeyForge - Password Generator

Generate strong passwords with ease using this Flutter app. Customize password length and character sets to meet your security needs.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [License](#license)
- [Contributing](#contributing)
- [Acknowledgments](#acknowledgments)

## Introduction

Welcome to the KeyForge - Password Generator app! This app allows you to create strong and secure passwords for your online accounts. It offers flexibility in customizing the password's length and character sets, ensuring your data remains protected.

## Features

- Generates strong passwords with customizable length and character sets.
- Options to include uppercase letters, lowercase letters, numbers, and symbols.
- Password is automatically copied to the clipboard for easy use.

## Getting Started

To run this project locally, you'll need to have Flutter installed. If you haven't already, follow the [Flutter installation guide](https://flutter.dev/docs/get-started/install) to get started.

1. Clone this repository:

   ```sh
   git clone https://gitlab.com/robinroy1370/keyforge.git
   ```

2. Navigate to the project directory:

        cd keyforge
3. Ensure you have Flutter and Dart installed. If not, refer to the official Flutter
       documentation or installation instructions.
  
4. Run the app on your preferred emulator or connected device using the following command:

        flutter run

## Usage

Launch the app.
Enter the desired password length.
Toggle the options to include uppercase letters, lowercase letters, numbers, and symbols.
Click the "Generate Password" button.
The generated password will be copied to your clipboard and displayed on the screen.

## License

This project is licensed under the GNU General Public License (GPL) 

## Contributing

We welcome contributions to improve this project. To contribute, follow these steps:

Fork the repository.
Create a new branch for your feature or bug fix.
Make your changes and commit them with clear messages.
Submit a pull request with a detailed description of your changes.

## Acknowledgments

Thanks to the Flutter community for their support and resources.

## Contact Information

If you have questions or feedback, feel free to reach out to :

    robinroy1370@gmail.com
