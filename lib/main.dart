import 'package:flutter/material.dart';
import 'package:keyforge/password_generator_app.dart';

void main() {
  runApp(const PasswordGeneratorApp());
}
