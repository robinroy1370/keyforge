import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'button.dart';
import 'dart:math';

class PasswordGeneratorScreen extends StatefulWidget {
  const PasswordGeneratorScreen({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _PasswordGeneratorScreenState createState() =>
      _PasswordGeneratorScreenState();
}

class _PasswordGeneratorScreenState extends State<PasswordGeneratorScreen> {
  final TextEditingController _lengthController = TextEditingController();
  bool _includeUppercase = true;
  bool _includeLowercase = true;
  bool _includeNumbers = true;
  bool _includeSymbols = true;

  String _generatedPassword = '';

  void _generatePassword() {
    int length = int.tryParse(_lengthController.text) ?? 12;
    String charset = '';

    if (_includeUppercase) charset += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (_includeLowercase) charset += 'abcdefghijklmnopqrstuvwxyz';
    if (_includeNumbers) charset += '0123456789';
    if (_includeSymbols) charset += '!@#\$%^&*()-_=+[]{}|;:,.<>?';

    String password = '';
    final random = Random();

    for (int i = 0; i < length; i++) {
      password += charset[random.nextInt(charset.length)];
    }

    setState(() {
      _generatedPassword = password;
    });

    Clipboard.setData(ClipboardData(
        text: password)); // Copy the generated password to the clipboard

    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text(
          'Password copied to clipboard',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.teal, // Set background color
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TextFormField(
            controller: _lengthController,
            keyboardType: TextInputType.number,
            decoration: const InputDecoration(
              labelText: 'Password Length',
              hintText: 'Enter length',
              labelStyle: TextStyle(color: Colors.teal),
              hintStyle: TextStyle(color: Colors.grey),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.teal),
              ),
            ),
          ),
          const SizedBox(height: 16),
          _buildOption(
            'Include Uppercase Letters',
            _includeUppercase,
            (value) => setState(() => _includeUppercase = value),
          ),
          _buildOption(
            'Include Lowercase Letters',
            _includeLowercase,
            (value) => setState(() => _includeLowercase = value),
          ),
          _buildOption(
            'Include Numbers',
            _includeNumbers,
            (value) => setState(() => _includeNumbers = value),
          ),
          _buildOption(
            'Include Symbols',
            _includeSymbols,
            (value) => setState(() => _includeSymbols = value),
          ),
          const ListTile(
            title: Text(
              '(Password will be automatically copied to the clipboard)',
              style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 16),
          MyButton(
            text: 'Generate Password',
            onPressed: _generatePassword,
          ),
          const SizedBox(height: 20),
          const Text(
            'Generated Password:',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.teal,
            ),
          ),
          const SizedBox(height: 10),
          SelectableText(
            _generatedPassword,
            style: const TextStyle(
              fontSize: 24,
              fontFamily: 'Monospace',
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildOption(String title, bool value, ValueChanged<bool> onChanged) {
    return ListTile(
      title: Text(
        title,
        style: const TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
      trailing: Switch(
        value: value,
        onChanged: onChanged,
        activeColor: Colors.teal,
      ),
    );
  }
}
