import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed; // Add this line

  const MyButton({Key? key, required this.text, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 62,
      decoration: BoxDecoration(
        color: Colors.teal,
        border: Border.all(color: Colors.white12, width: 2.0),
        borderRadius: BorderRadius.circular(40),
      ),
      padding: const EdgeInsets.all(4),
      child: InkWell(
        // Use InkWell for tap effect
        onTap: onPressed, // Assign the onPressed callback
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              text,
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
            
          ],
        ),
      ),
    );
  }
}

